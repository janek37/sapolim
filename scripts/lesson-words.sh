#!/bin/bash

if [ -n "$1" ]; then
    filename="$1"
else
    filename=textbook.asciidoc
fi

grep -o "[^a-zA-Z}]'[a-zA-Z][^']\+'\|^'[^']\{2,\}'\| Lesson [0-9]\+" "$filename" | sed "s/^[^']//"

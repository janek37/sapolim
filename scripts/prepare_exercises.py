#!/usr/bin/env python
import re
import sys

def exercises(input_file, answers=False):
    output = ['=== Exercises ===']
    current = 0
    for line in input_file:
        line = line.decode('utf-8').strip('\n')
        if line.startswith('==='):
            current += 1
            if current > 1:
                output.append('endif::[]')
            output.append('ifeval::[{exercisesnum}==%s]' % current)
            continue
        if answers:
            line = re.sub(
                r'tr:\[(.*)\]\[(.*)\]', r'\1\n+\n{% *Toggle answer* %}["\2"]', line)
        else:
            line = re.sub(r'tr:\[(.*)\]\[(.*)\]', r'\1', line)
        output.append(line)
    output.append('endif::[]')
    return '\n'.join(output)

def answers(input_file):
    output = ['== Answers ==', '']
    for line in input_file:
        line = line.decode('utf-8').strip('\n')
        match = re.match(r'([0-9]*\.) ', line)
        if match:
            output.append(match.group(1))
            continue
        line = re.sub(r'tr:\[(.*)\]\[(.*)\]', r'\2', line)
        output.append(line)
    return '\n'.join(output)

if __name__ == '__main__':
    filename = sys.argv[1]
    if sys.argv[2] == '-e':
        answers = '-a' in sys.argv[3:]
        print exercises(open(filename), answers).encode('utf-8')
    else:
        print answers(open(filename)).encode('utf-8')

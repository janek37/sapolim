from dparser import Parser

def d_sentence(t):
    """S : 'ikosel'? VP (C VP)* (RB M1P+)?"""
    return {
        'polite': bool(t[0]),
        'verb_phrase': t[1],
        'conj_segments': [{'conj': c, 'verb_phrase': vp} for c, vp in t[2]],
        'bracket_mods': t[3][1] if t[3] else [],
        'type': 'S',
    }

def d_verb_phrase(t):
    """VP : ('ek' | 'im')? V M1P*"""
    return {
        'marker': t[0][0][0] if t[0] else None,
        'verb': t[1],
        'mods': t[2],
        'type': 'VP',
    }

def d_mod_phrase(t):
    """M1P : TAG* M1A (C M1A)* (RB M2P+)?;
       M2P : TAG* M2A (C M2A)* (RB M3P+)?;
       M3P : TAG* M3A (C M3A)* (RB M4P+)?"""
    return {
        'tags': t[0],
        'mod': t[1]['mod'],
        'mods': t[1]['mods'],
        'conj_segments': [{'conj': c, 'mod': mod['mod'], 'mods': mod['mods']}
                          for c, mod in t[2]],
        'bracket_mods': t[3][1] if t[3] else [],
        'type': 'MP',
    }

def d_mod_phrase_a(t):
    """M1A : M1 M2P*;
       M2A : M2 M3P*;
       M3A : M3 M3P*"""
    return {'mod': t[0], 'mods': t[1]}

def d_last_mod_phrase(t):
    """M4P : TAG* M4 (C M4)*"""
    return {
        'tags': t[0],
        'mod': t[1],
        'mods': [],
        'conj_segments': [{'conj': c, 'mod': mod, 'mods': []}
                          for c, mod in t[2]],
        'bracket_mods': [],
        'type': 'MP',
    }

def d_word(t):
    """V  : PV? VWORD  PN* NUM*;
       M1 : PV? M1WORD PN* NUM*;
       M2 : PV? M2WORD PN* NUM*;
       M3 : PV? M3WORD PN* NUM*;
       M4 : PV? M4WORD PN* NUM*"""
    return {
        'passive': bool(t[0]),
        'word': t[1],
        'name': t[2],
        'number': t[3],
    }

# C, RB, Q, IMP, TAG, PV, PN, NUM

def d_vword(t):
    '''VWORD : "[klmpst]a[klmpst]([aeiou][klmpst])*"'''
    return t[0]

def d_m1word(t):
    '''M1WORD : "[klmpst]e[klmpst]([aeiou][klmpst])*"'''
    return t[0]

def d_m2word(t):
    '''M2WORD : "[klmpst]i[klmpst]([aeiou][klmpst])*"'''
    return t[0]

def d_m3word(t):
    '''M3WORD : "[klmpst]o[klmpst]([aeiou][klmpst])*"'''
    return t[0]

def d_m4word(t):
    '''M4WORD : "[klmpst]u[klmpst]([aeiou][klmpst])*"'''
    return t[0]

def d_tag(t):
    '''TAG : "ak|op|es|am|at|il" '''
    return t[0]

def d_conj(t):
    '''C : "is|ol|ep|ut|al" '''
    return t[0]

def d_bracket(t):
    """RB : 'uk' """
    return t[0]

def d_abstraction(t):
    """ABS: 'it' """
    return t[0]

def d_passive(t):
    """PV : 'om' """
    return t[0]

def d_name(t):
    '''PN : "[A-Z][A-Za-z]*" '''
    return t[0]

def d_digit(t):
    '''NUM : "akel|elim|imop|opus|usat|atek|ekis|isom|omut|utak|us" '''
    return t[0]

parser = Parser()

def parse(s):
    return parser.parse(s)

def prepare_word(word):
    w = ' '.join([word['word']] + word['name'] + word['number'])
    if word['passive']:
        w = 'om ' + w
    return w

def simple_tree(s):
    t = s['type']
    if t == 'S':
        label = 'sentence'
        children = []
        if s['polite']:
            children.append(('ikosel', []))
        children.append(simple_tree(s['verb_phrase']))
        for seg in s['conj_segments']:
            children.append((seg['conj'], []))
            children.append(simple_tree(seg['verb_phrase']))
    elif t == 'VP':
        label = 'verb phrase'
        children = []
        if s['marker']:
            children.append((s['marker'], []))
        children.append((prepare_word(s['verb']), [
            simple_tree(mod) for mod in s['mods']]))
    elif t == 'MP':
        label = 'mod phrase'
        children = []
        for tag in s['tags']:
            children.append((tag, []))
        children.append((prepare_word(s['mod']), [
            simple_tree(mod) for mod in s['mods']]))
        for seg in s['conj_segments']:
            children.append((seg['conj'], []))
            children.append((prepare_word(seg['mod']), [
                simple_tree(mod) for mod in seg['mods']]))
    
    if t in ('V', 'MP'):
        if s['bracket_mods']:
            children.append(('uk', [
                simple_tree(mod) for mod in s['bracketed_mods']]))
    return (label, children)


if __name__ == '__main__':
    import sys
    print simple_tree(parse(sys.argv[1]).structure)


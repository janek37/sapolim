PYTHON=python2.7
ASCIIDOC=asciidoc
WAXEYE=waxeye
HTML_FLAGS=-b html5 -a icons -a data-uri
BOOTSTRAP_FLAGS=-b bootstrap -f bootstrap3.conf -a scriptsdir=javascripts \
	-a stylesdir=stylesheets
DOCBOOK_FLAGS=-b docbook
HTML_CONF=asciidoc.conf html5.conf
BOOTSTRAP_CONF=asciidoc.conf bootstrap3.conf
DOCBOOK_CONF=asciidoc.conf

all: output_dir bootstrap waxeye-js

output_dir:
	mkdir -p gen/bootstrap

waxeye-js: gen/parser.js

gen/parser.js: waxeye/sapolim.waxeye
	$(WAXEYE) -g javascript gen $<

html: output_dir gen/description.html gen/vocabulary.html gen/textbook.html \
		gen/index.html

bootstrap: gen/bootstrap/description.html gen/bootstrap/vocabulary.html \
		gen/bootstrap/textbook.html gen/bootstrap/index.html

docbook: gen/description.xml gen/vocabulary.xml gen/textbook.xml

gen/textbook.html: textbook.asciidoc version.asciidoc exercises-a.gen $(HTML_CONF)
	$(ASCIIDOC) -o $@ $(HTML_FLAGS) $<

gen/bootstrap/textbook.html: textbook.asciidoc version.asciidoc \
                             exercises-a.gen $(BOOTSTRAP_CONF)
	$(ASCIIDOC) -o $@ $(BOOTSTRAP_FLAGS) $<

gen/textbook.xml: textbook.asciidoc version.asciidoc exercises.gen \
                  answers.gen $(DOCBOOK_CONF)
	$(ASCIIDOC) -o $@ $(DOCBOOK_FLAGS) $<

exercises-a.gen: scripts/prepare_exercises.py exercises.txt
	$(PYTHON) $^ -e -a > $@

exercises.gen: scripts/prepare_exercises.py exercises.txt
	$(PYTHON) $^ -e > $@

answers.gen: scripts/prepare_exercises.py exercises.txt
	$(PYTHON) $^ -a > $@

gen/%.html: %.asciidoc version.asciidoc $(HTML_CONF)
	$(ASCIIDOC) -o $@ $(HTML_FLAGS) $<

gen/bootstrap/%.html: %.asciidoc version.asciidoc $(BOOTSTRAP_CONF)
	$(ASCIIDOC) -o $@ $(BOOTSTRAP_FLAGS) $<

gen/%.xml: %.asciidoc version.asciidoc $(DOCBOOK_CONF)
	$(ASCIIDOC) -o $@ $(DOCBOOK_FLAGS) $<

clean:
	rm -rf gen
	rm -f *.gen
